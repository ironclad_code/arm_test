// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.io.File;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.Mechanism2d;
import edu.wpi.first.wpilibj.smartdashboard.MechanismLigament2d;
import edu.wpi.first.wpilibj.smartdashboard.MechanismRoot2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj.util.Color8Bit;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.subsystems.SwerveSubsystem;
import frc.robot.subsystems.arm.ArmSubsystem;
import frc.robot.subsystems.arm.ArmTolerances;
import frc.robot.subsystems.arm.PhysicalProperties;
import frc.robot.subsystems.arm.ArmPose;

public class RobotContainer {

  private double armHeight = 0.3;

  CommandXboxController controller;
  Field2d field;

  SwerveSubsystem swerve;
  ArmSubsystem arm;

  Mechanism2d mech = new Mechanism2d(4, 4);
  MechanismRoot2d root = mech.getRoot("root", 2, armHeight);
  MechanismLigament2d armLigament = root.append(new MechanismLigament2d("arm", 1, 0, 6, new Color8Bit(Color.kPurple)));
  MechanismLigament2d wrisLigament = armLigament.append(new MechanismLigament2d("wrist", 0.2, 0, 4, new Color8Bit(Color.kBlue)));

  public RobotContainer() {
    controller = new CommandXboxController(0);
    swerve = new SwerveSubsystem(new File(Filesystem.getDeployDirectory(), "swerve"), () -> false);
    arm = new ArmSubsystem(swerve::getPose, new ArmTolerances(2, 2, 2, 1), new PhysicalProperties(new Translation3d(0, 0, armHeight), 0, 0));

    swerve.setDefaultCommand(swerve.teleopCommand(() -> controller.getLeftX(), () -> -controller.getLeftY(), () -> -controller.getRightX(), controller::getRightTriggerAxis));

    arm.setDefaultCommand(arm.home());

    controller.y().toggleOnTrue(arm.setArmPose(new ArmPose(Rotation2d.fromDegrees(90), true, new Rotation2d(), 0, new Rotation2d())));
    controller.x().toggleOnTrue(arm.setArmPose(new ArmPose(Rotation2d.fromDegrees(180), true, new Rotation2d(), 0, new Rotation2d())));
    controller.a().toggleOnTrue(arm.setArmPose(new ArmPose(Rotation2d.fromDegrees(-90), true, new Rotation2d(), 0, new Rotation2d())));
    controller.b().toggleOnTrue(arm.setArmPose(new ArmPose(new Rotation2d(), true, new Rotation2d(), 0, new Rotation2d())));

    controller.leftBumper().whileTrue(arm.setArmPose(new ArmPose(new Rotation2d(Math.toRadians(405)), false, Rotation2d.fromDegrees(45), 0, Rotation2d.fromDegrees(-45))));
    controller.rightBumper().whileTrue(arm.setArmPose(new ArmPose(new Rotation2d(Math.toRadians(-45)), false, Rotation2d.fromDegrees(-100), 0, Rotation2d.fromDegrees(10))));

    controller.start().toggleOnTrue(arm.stickToPosition(new Translation3d(8.259071, 4.144347, 5)));

    controller.back().toggleOnTrue(swerve.RotatTwardsPose(() -> controller.getLeftX(), () -> -controller.getLeftY(), controller::getRightTriggerAxis, () -> new Pose2d(5, 5, new Rotation2d()), () -> new Rotation2d()));

    arm.atMaxSpin().onTrue(arm.unwind());
  }

  public Command getAutonomousCommand() {
    return Commands.print("No autonomous command configured");
  }

  public void render() {
    drawField();
    drawMechanism();
  }

  private void drawField() {
    field = swerve.getField();
    ArmPose armPose = arm.getArmPose(true);
    field.getObject("arm").setPose(new Pose2d(swerve.getPose().getX(), swerve.getPose().getY(), armPose.getRotation()));
  }

  private void drawMechanism() {
    ArmPose pose = arm.getArmPose(false);
    armLigament.setAngle(pose.getShoulderRotation().getDegrees() + 90);
    wrisLigament.setAngle(pose.getWristRotation().getDegrees());
    SmartDashboard.putData("Arm Mechanism", mech);
  }
}
