package frc.robot.utils;

import java.util.Optional;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;

public class AprilTagUtils {
    /**
     * Return the ID of an aprilTag for your current alliance
     * @param aprilTagPlace
     * @return ID of the alliance specifc aprilTag
     */
    public static int getAllianceRelativeID(AprilTagPositions aprilTagPlace) {
        Optional<Alliance> ally = DriverStation.getAlliance();
        if(ally.isPresent()) {
            if (ally.get() == Alliance.Blue) {
                switch (aprilTagPlace) {
                    case SpeakerCenter: return 7;
                    case Amp: return 6;
                    case SourceLeft: return 2;
                    case SourceRight: return 1;
                    case SpeakerRight: return 8;
                    case StageCenter: return 14;
                    case StageLeft: return 15;
                    case StageRight: return 16;
                }
            } else if (ally.get() == Alliance.Red) {
                switch (aprilTagPlace) {
                    case SpeakerCenter: return 4;
                    case Amp: return 5;
                    case SourceLeft: return 10;
                    case SourceRight: return 9;
                    case SpeakerRight: return 3;
                    case StageCenter: return 13;
                    case StageLeft: return 11;
                    case StageRight: return 12;
                }
            }
        }
        //uh oh
        return -1;
    }

}

