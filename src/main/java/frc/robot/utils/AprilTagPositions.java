package frc.robot.utils;

public enum AprilTagPositions {
    SpeakerCenter,
    SpeakerRight,
    Amp,
    SourceLeft,
    SourceRight,
    StageCenter,
    StageLeft,
    StageRight
}
