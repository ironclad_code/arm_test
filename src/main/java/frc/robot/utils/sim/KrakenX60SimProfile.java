package frc.robot.utils.sim;

import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.sim.TalonFXSimState;

import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.simulation.DCMotorSim;
import frc.robot.utils.sim.PhysicsSim.SimProfile;

public class KrakenX60SimProfile extends SimProfile {
    private static final double motorResistance = 0.002; // Assume 2mOhm resistance for voltage drop calculation
    private final TalonFXSimState krakenSim;
    private final DCMotorSim motorSim;

    public KrakenX60SimProfile(final TalonFX kraken, final boolean foc, final double rotorInertia) {
        if(foc) {
            this.motorSim = new DCMotorSim(DCMotor.getKrakenX60Foc(1), 1.0, rotorInertia);
            this.krakenSim = kraken.getSimState();
        } else {
            this.motorSim = new DCMotorSim(DCMotor.getKrakenX60(1), 1.0, rotorInertia);
            this.krakenSim = kraken.getSimState();
        }
    }

    /**
     * Runs the simulation profile.
     * 
     * This uses very rudimentary physics simulation and exists to allow users to
     * test features of our products in simulation using our examples out of the
     * box. Users may modify this to utilize more accurate physics simulation.
     */
    public void run() {
        /// DEVICE SPEED SIMULATION

        motorSim.setInputVoltage(krakenSim.getMotorVoltage());

        motorSim.update(getPeriod());

        /// SET SIM PHYSICS INPUTS
        final double position_rot = motorSim.getAngularPositionRotations();
        final double velocity_rps = Units.radiansToRotations(motorSim.getAngularVelocityRadPerSec());

        krakenSim.setRawRotorPosition(position_rot);
        krakenSim.setRotorVelocity(velocity_rps);

        krakenSim.setSupplyVoltage(12 - krakenSim.getSupplyCurrent() * motorResistance);
    }
}
