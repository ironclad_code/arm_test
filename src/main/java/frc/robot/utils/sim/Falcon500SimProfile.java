package frc.robot.utils.sim;

import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.sim.TalonFXSimState;

import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.simulation.DCMotorSim;
import frc.robot.utils.sim.PhysicsSim.SimProfile;


/**
 * Holds information about a simulated Falcon 500.
 */
class Falcon500SimProfile extends SimProfile {
    private static final double motorResistance = 0.002; // Assume 2mOhm resistance for voltage drop calculation
    private final TalonFXSimState falconSim;
    private final DCMotorSim motorSim;

    /**
     * Creates a new simulation profile for a TalonFX device.
     * 
     * @param falcon
     *                        The TalonFX device
     * @param rotorInertia
     *                        Rotational Inertia of the mechanism at the rotor
     */
    public Falcon500SimProfile(final TalonFX falcon, final double rotorInertia, boolean foc) {
        if(foc) {
            this.motorSim = new DCMotorSim(DCMotor.getFalcon500Foc(1), 1.0, rotorInertia);
            this.falconSim = falcon.getSimState();
        } else {
            this.motorSim = new DCMotorSim(DCMotor.getFalcon500(1), 1.0, rotorInertia);
            this.falconSim = falcon.getSimState();
        }
    }

    /**
     * Runs the simulation profile.
     * 
     * This uses very rudimentary physics simulation and exists to allow users to
     * test features of our products in simulation using our examples out of the
     * box. Users may modify this to utilize more accurate physics simulation.
     */
    public void run() {
        /// DEVICE SPEED SIMULATION

        motorSim.setInputVoltage(falconSim.getMotorVoltage());

        motorSim.update(getPeriod());

        /// SET SIM PHYSICS INPUTS
        final double position_rot = motorSim.getAngularPositionRotations();
        final double velocity_rps = Units.radiansToRotations(motorSim.getAngularVelocityRadPerSec());

        falconSim.setRawRotorPosition(position_rot);
        falconSim.setRotorVelocity(velocity_rps);

        falconSim.setSupplyVoltage(12 - falconSim.getSupplyCurrent() * motorResistance);
    }
}