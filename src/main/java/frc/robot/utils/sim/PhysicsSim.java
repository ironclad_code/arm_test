package frc.robot.utils.sim;

import java.util.ArrayList;

import com.ctre.phoenix6.Utils;
import com.ctre.phoenix6.hardware.TalonFX;

/**
 * Manages physics simulation for CTRE products.
 */
public class PhysicsSim {
    private static final PhysicsSim sim = new PhysicsSim();

    /**
     * Gets the robot simulator instance.
     */
    public static PhysicsSim getInstance() {
        return sim;
    }

    /**
     * Adds a TalonFX controller to the simulator.
     * 
     * @param falcon
     *        The TalonFX device
     * @param rotorInertia
     *        Rotational Inertia of the mechanism at the rotor
     */
    public void addFalcon(TalonFX falcon, boolean foc, final double rotorInertia) {
        if (falcon != null) {
            Falcon500SimProfile simFalcon = new Falcon500SimProfile(falcon, rotorInertia, foc);
            _simProfiles.add(simFalcon);
        }
    }

    /**
     * Adds a TalonFX controller to the simulator.
     * 
     * @param falcon
     *        The TalonFX device
     * @param foc
     *        If the device uses foc
     */
    public void addFalcon(TalonFX falcon, boolean foc) {
        addFalcon(falcon, foc, 0.001);
    }

    public void addFalcon(TalonFX falcon) {
        addFalcon(falcon, false, 0.001);
    }

    public void addKraken(TalonFX kraken, final boolean foc, final double rotorInertia) {
        if (kraken != null) {
            KrakenX60SimProfile simFalcon = new KrakenX60SimProfile(kraken, foc, rotorInertia);
            _simProfiles.add(simFalcon);
        }
    }

    public void addKraken(TalonFX kraken, final boolean foc) {
        addKraken(kraken, foc, 0.001);
    }

    public void addKraken(TalonFX kraken) {
        addKraken(kraken, false, 0.001);
    }

    /**
     * Runs the simulator:
     * - enable the robot
     * - simulate sensors
     */
    public void run() {
        // Simulate devices
        for (SimProfile simProfile : _simProfiles) {
            simProfile.run();
        }
    }

    private final ArrayList<SimProfile> _simProfiles = new ArrayList<SimProfile>();

    /**
     * Holds information about a simulated device.
     */
    static class SimProfile {
        private double _lastTime;
        private boolean _running = false;

        /**
         * Runs the simulation profile.
         * Implemented by device-specific profiles.
         */
        public void run() {
        }

        /**
         * Returns the time since last call, in seconds.
         */
        protected double getPeriod() {
            // set the start time if not yet running
            if (!_running) {
                _lastTime = Utils.getCurrentTimeSeconds();
                _running = true;
            }

            double now = Utils.getCurrentTimeSeconds();
            final double period = now - _lastTime;
            _lastTime = now;

            return period;
        }
    }
}