package frc.robot.subsystems.arm;

import javax.management.RuntimeErrorException;

import edu.wpi.first.math.geometry.Translation3d;

public class PhysicalProperties {
    private Translation3d armPosition;
    private double armMinimumLength;
    private double armMaximumLength;

    public PhysicalProperties(Translation3d armPosition, double armMinimumLength, double armMaximumLength) {
        this.armPosition = armPosition;
        this.armMinimumLength = armMaximumLength;
        this.armMaximumLength = armMaximumLength;
        if(armMinimumLength > armMaximumLength) throw new RuntimeErrorException(new Error(), "arm minimum length cannot be greater than maximum length");
    }

    public Translation3d getArmPosition() {
        return armPosition;
    }

    public void setArmPosition(Translation3d armPosition) {
        this.armPosition = armPosition;
    }

    public double getArmMinimumLength() {
        return armMinimumLength;
    }

    public void setArmMinimumLength(double armMinimumLength) {
        this.armMinimumLength = armMinimumLength;
    }

    public double getArmMaximumLength() {
        return armMaximumLength;
    }

    public void setArmMaximumLength(double armMaximumLength) {
        this.armMaximumLength = armMaximumLength;
    }
}
