package frc.robot.subsystems.arm;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Rotation2d;

public class ArmPose {
    private Rotation2d rotation;
    private Rotation2d shoulderRotation;
    private double liniarPos;
    private Rotation2d wristRotation;
    private boolean fieldReletive;

    public ArmPose() {
        this(new Rotation2d(), false, new Rotation2d(), 0, new Rotation2d());
    }

    public ArmPose(Rotation2d rotation, boolean fieldReletive, Rotation2d shoulderRotation, double liniarPos, Rotation2d wristRotation) {
        this.fieldReletive = fieldReletive;
        this.liniarPos = liniarPos;
        this.rotation = clampRotation(rotation);
        this.shoulderRotation = clampRotation(shoulderRotation);
        this.wristRotation = clampRotation(wristRotation);
    }

    public Rotation2d getRotation() {
        return rotation;
    }

    public void setRotation(Rotation2d rotation) {
        this.rotation = rotation;
    }

    public Rotation2d getShoulderRotation() {
        return shoulderRotation;
    }

    public void setShoulderRotation(Rotation2d shoulderRotation) {
        this.shoulderRotation = shoulderRotation;
    }

    public double getLiniarPos() {
        return liniarPos;
    }

    public void setLiniarPos(double liniarPos) {
        this.liniarPos = liniarPos;
    }

    public Rotation2d getWristRotation() {
        return wristRotation;
    }

    public void setWristRotation(Rotation2d wristRotation) {
        this.wristRotation = wristRotation;
    }

    public boolean isFieldReletive() {
        return fieldReletive;
    }

    public void setFieldReletive(boolean fieldReletive) {
        this.fieldReletive = fieldReletive;
    }

    public ArmPose inverse() {
        return new ArmPose(rotation.unaryMinus(), fieldReletive, shoulderRotation.unaryMinus(), -liniarPos, wristRotation.unaryMinus());
    }

    private Rotation2d clampRotation(Rotation2d rotation) {
        return Rotation2d.fromDegrees(MathUtil.inputModulus(rotation.getDegrees(), -180, 180));
    }

    @Override
    public String toString() {
        return new StringBuilder("rotation: ").append(rotation.getDegrees()).append(" shoulder: ").append(shoulderRotation.getDegrees()).append(" liniarPos: ").append(liniarPos).append(" wrist: ").append(wristRotation.getDegrees()).toString();
    }
}
