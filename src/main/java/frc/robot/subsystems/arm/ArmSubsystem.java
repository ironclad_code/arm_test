package frc.robot.subsystems.arm;

import java.util.function.Supplier;

import com.ctre.phoenix6.configs.Slot0Configs;
import com.ctre.phoenix6.configs.TalonFXConfiguration;
import com.ctre.phoenix6.controls.PositionDutyCycle;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.NeutralModeValue;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.filter.Debouncer.DebounceType;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.utils.sim.PhysicsSim;

public class ArmSubsystem extends SubsystemBase {
    private TalonFX rotationMotor;
    private TalonFX shoulderMotor;
    private TalonFX liniarMotor;
    private TalonFX wristMotor;

    private PositionDutyCycle rotationCycle;
    private PositionDutyCycle shoulderCycle;
    private PositionDutyCycle wristCycle;
    private PositionDutyCycle liniarCycle;

    private ArmTolerances armTolerances;
    private PhysicalProperties physicalProperties;

    private ArmPose desiredPose;

    Supplier<Pose2d> robotPose;

    private double previous_arm_rotation_degrees = 0;
    private Debouncer debouncer = new Debouncer(0.3, DebounceType.kRising);

    private boolean commandScheduleFix = false;
    private int commandScheduleFixTimer = 0;

    public ArmSubsystem(Supplier<Pose2d> robotPose, ArmTolerances armTolerances, PhysicalProperties physicalProperties) {
        this.rotationMotor = new TalonFX(0);
        this.liniarMotor = new TalonFX(1);
        this.shoulderMotor = new TalonFX(2);
        this.wristMotor = new TalonFX(3);
        this.robotPose = robotPose;
        this.armTolerances = armTolerances;
        this.physicalProperties = physicalProperties;

        desiredPose = new ArmPose();

        TalonFXConfiguration factoryConfig = new TalonFXConfiguration();
        rotationMotor.getConfigurator().apply(factoryConfig);
        liniarMotor.getConfigurator().apply(factoryConfig);
        shoulderMotor.getConfigurator().apply(factoryConfig);
        wristMotor.getConfigurator().apply(factoryConfig);
        
        Slot0Configs pidConfigs = new Slot0Configs();
        pidConfigs.kP = 0.05;
        pidConfigs.kD = 0;
        pidConfigs.kI = 0;

        rotationMotor.getConfigurator().apply(pidConfigs);
        liniarMotor.getConfigurator().apply(pidConfigs);
        shoulderMotor.getConfigurator().apply(pidConfigs);
        wristMotor.getConfigurator().apply(pidConfigs);
        
        PhysicsSim.getInstance().addKraken(rotationMotor);
        PhysicsSim.getInstance().addKraken(liniarMotor);
        PhysicsSim.getInstance().addKraken(shoulderMotor);
        PhysicsSim.getInstance().addKraken(wristMotor);

        rotationMotor.setNeutralMode(NeutralModeValue.Brake);
        liniarMotor.setNeutralMode(NeutralModeValue.Brake);
        shoulderMotor.setNeutralMode(NeutralModeValue.Brake);
        wristMotor.setNeutralMode(NeutralModeValue.Brake);

        rotationCycle = new PositionDutyCycle(0);
        shoulderCycle = new PositionDutyCycle(0);
        wristCycle = new PositionDutyCycle(0);
        liniarCycle = new PositionDutyCycle(0);
    }

    public ArmPose getDesiredPose() {
        return desiredPose;
    }

    private Rotation2d getInternalRotation() {
        return Rotation2d.fromDegrees(rotationMotor.getPosition().getValue() * 360);
    }

    private Rotation2d getArmRotation(boolean fieldReletive) {
        double angleDegrees = MathUtil.inputModulus(getInternalRotation().getDegrees(), -180, 180);
        Rotation2d rotation = Rotation2d.fromDegrees(angleDegrees);
        if(fieldReletive) return robotPose.get().getRotation().plus(Rotation2d.fromDegrees(angleDegrees));
        return rotation;
    }

    private Rotation2d getShoulderRotation() {
        return new Rotation2d(Math.toRadians(shoulderMotor.getPosition().getValue() * 360));
    }

    private Rotation2d getWristRotation() {
        return new Rotation2d(Math.toRadians(wristMotor.getPosition().getValue() * 360));
    }

    //idk how i want to interperate this yet
    private double getExtensionDistance() {
        return liniarMotor.getPosition().getValue();
    }

    public ArmPose getArmPose(boolean fieldReletive) {
        return new ArmPose(getArmRotation(fieldReletive), false, getShoulderRotation(), getExtensionDistance(), getWristRotation());
    }

    public void stop() {
        rotationMotor.stopMotor();
        shoulderMotor.stopMotor();
        liniarMotor.stopMotor();
        wristMotor.stopMotor();
    }

    private void setArmRotation(Rotation2d rotation) {
        double arm_rotation_degrees = rotation.getDegrees();
        arm_rotation_degrees += Math.ceil((previous_arm_rotation_degrees - 180) / 360) * 360;
        double delta = Math.abs(arm_rotation_degrees - previous_arm_rotation_degrees);

        double option = arm_rotation_degrees + 360;
        double option_delta = Math.abs(option - previous_arm_rotation_degrees);
        if (option_delta < delta) {
            arm_rotation_degrees = option;
            delta = option_delta;
        }

        option = arm_rotation_degrees - 360;
        option_delta = Math.abs(option - previous_arm_rotation_degrees);
        if (option_delta < delta) {
            arm_rotation_degrees = option;
            delta = option_delta;
        }

        rotationMotor.setControl(rotationCycle.withPosition(arm_rotation_degrees / 360));
        previous_arm_rotation_degrees = arm_rotation_degrees;
    }

    private void setShoulderRotation(Rotation2d shoulderRotation) {
        shoulderMotor.setControl(shoulderCycle.withPosition(shoulderRotation.getDegrees() / 360));
    }

    private void setExtension(double pos) {
        liniarMotor.setControl(liniarCycle.withPosition(pos));
    }

    private void setWristRotation(Rotation2d rotation) {
        wristMotor.setControl(wristCycle.withPosition(rotation.getDegrees() / 360));
    }

    public double getSpinCount() {
        return getInternalRotation().getRotations();
    }

    public ArmTolerances getArmTolerances() {
        return armTolerances;
    }

    public void setArmTolerances(ArmTolerances armTolerances) {
        this.armTolerances = armTolerances;
    }

    public boolean armAtPosition() {
        boolean rotationAtPosition = Math.abs(rotationMotor.getClosedLoopError().getValue()) * 360 < armTolerances.getRotationTolerance();
        boolean shoulderAtPosition = Math.abs(shoulderMotor.getClosedLoopError().getValue()) * 360 < armTolerances.getShoulderTolerance();
        boolean extentionAtPosition = true;
        boolean wristAtPosition = Math.abs(wristMotor.getClosedLoopError().getValue()) * 360 < armTolerances.getWristTolerance();

        if(commandScheduleFix) return false;
        return debouncer.calculate(rotationAtPosition && shoulderAtPosition && extentionAtPosition && wristAtPosition);
    }

    public Trigger armArrivedAtPosition() {
        return new Trigger(this::armAtPosition);
    }

    public Trigger atMaxSpin() {
        return new Trigger(() -> Math.abs(getSpinCount()) >= armTolerances.getRotationLimit());
    }

    public Command setArmPose(ArmPose pose) {
        return new FunctionalCommand(
            () -> {
                commandScheduleFix = true;
                desiredPose = pose;
            }, 
            () -> {
                if(!pose.isFieldReletive()) {
                    setArmRotation(pose.getRotation());   
                } else {
                    setArmRotation(pose.getRotation().minus(robotPose.get().getRotation()));
                }
                setShoulderRotation(pose.getShoulderRotation());
                setWristRotation(pose.getWristRotation());
                setExtension(pose.getLiniarPos());
            }, 
            (interrupted) -> {}, 
            () -> false, 
            this
        ).withName("Arm To Pose");
    }

    public Command stickToPosition(Translation3d position) {
        return new FunctionalCommand(
            () -> {
                commandScheduleFix = true;
                Pose2d robotPose = this.robotPose.get();
                Rotation2d rotation = new Rotation2d(position.getX() - robotPose.getX(), position.getY() - robotPose.getY());
                ArmPose pose = new ArmPose(rotation, true, new Rotation2d(), 0, new Rotation2d());
                desiredPose = pose;
            }, 
            () -> {
                Pose2d robotPose = this.robotPose.get();
                Rotation2d rotation = new Rotation2d(position.getX() - robotPose.getX(), position.getY() - robotPose.getY());
                ArmPose pose = new ArmPose(rotation, true, new Rotation2d(), 0, new Rotation2d());

                setArmRotation(pose.getRotation().minus(robotPose.getRotation()));
                setShoulderRotation(pose.getShoulderRotation());
                setWristRotation(pose.getWristRotation());
                setExtension(pose.getLiniarPos());
            }, 
            (interrupted) -> {}, 
            () -> false, 
            this
        ).withName("Stick To Position");
    }

    public Command home() {
        return new FunctionalCommand(
            () -> {
                commandScheduleFix = true;
            }, 
            () -> {
                setArmRotation(new Rotation2d());
                setShoulderRotation(new Rotation2d());
                setWristRotation(new Rotation2d());
                setExtension(0);
            }, 
            (interrupted) -> {}, 
            () -> false, 
            this
        ).withName("Home");
    }

    public Command unwind() {
        return run(() -> {
            previous_arm_rotation_degrees = 0;
            rotationMotor.setControl(rotationCycle.withPosition(0));
            setExtension(0);
            setShoulderRotation(new Rotation2d());
            setWristRotation(new Rotation2d());
        }).withName("Unwind Arm");
    }

    @Override
    public void periodic() {
        SmartDashboard.putData("Arm/Subsystem", this);
        SmartDashboard.putNumber("Arm/Absolute Rotation", getArmPose(true).getRotation().getDegrees());
        SmartDashboard.putNumber("Arm/Relitive Rotation", getArmPose(false).getRotation().getDegrees());
        
        SmartDashboard.putNumber("Arm/Shoulder Rotation", getShoulderRotation().getDegrees());
        SmartDashboard.putNumber("Arm/Wrist Rotation", getWristRotation().getDegrees());

        SmartDashboard.putNumber("Arm/Rotations", getSpinCount());

        SmartDashboard.putBoolean("Arm/At Position", armAtPosition());
        
        SmartDashboard.putBoolean("Arm/Command Fix/Fix", commandScheduleFix);
        SmartDashboard.putNumber("Arm/Command Fix/Timer", commandScheduleFixTimer);
        if(commandScheduleFix) {
            commandScheduleFixTimer++;
        }

        if(commandScheduleFixTimer >= 5) {
            commandScheduleFix = false;
            commandScheduleFixTimer = 0;
        }
    }
}
