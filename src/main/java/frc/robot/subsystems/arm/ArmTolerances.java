package frc.robot.subsystems.arm;

public class ArmTolerances {
    private double rotationTolerance;
    private double shoulderTolerance;
    private double wristTolerance;
    private double liniarTolerance;
    private double rotationLimit;

    public ArmTolerances(double rotationTolerance, double shoulderTolerance, double wristTolerance, double liniarTolerance, double rotationLimit) {
        this.shoulderTolerance = shoulderTolerance;
        this.rotationTolerance = rotationTolerance;
        this.wristTolerance = wristTolerance;
        this.liniarTolerance = liniarTolerance;
        this.rotationLimit = rotationLimit;
    }

    public ArmTolerances(double rotationTolerance, double shoulderTolerance, double wristTolerance, double liniarTolerance) {
        this(rotationTolerance, shoulderTolerance, wristTolerance, liniarTolerance, Double.POSITIVE_INFINITY);
    }

    public ArmTolerances() {
        this(0, 0, 0, 0, Double.POSITIVE_INFINITY);
    }

    public double getRotationLimit() {
        return rotationLimit;
    }

    public void setRotationLimit(double rotationLimit) {
        this.rotationLimit = rotationLimit;
    }

    public double getRotationTolerance() {
        return rotationTolerance;
    }

    public void setRotationTolerance(double rotationTolerance) {
        this.rotationTolerance = rotationTolerance;
    }

    public double getShoulderTolerance() {
        return shoulderTolerance;
    }

    public void setShoulderTolerance(double shoulderTolerance) {
        this.shoulderTolerance = shoulderTolerance;
    }

    public double getWristTolerance() {
        return wristTolerance;
    }

    public void setWristTolerance(double wristTolerance) {
        this.wristTolerance = wristTolerance;
    }

    public double getLiniarTolerance() {
        return liniarTolerance;
    }

    public void setLiniarTolerance(double liniarTolerance) {
        this.liniarTolerance = liniarTolerance;
    }
}
