package frc.robot.subsystems;

import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;
import java.util.function.Supplier;

import com.ctre.phoenix6.hardware.TalonFX;
import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.controllers.PPHolonomicDriveController;
import com.pathplanner.lib.path.PathConstraints;
import com.pathplanner.lib.path.PathPlannerPath;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import swervelib.SwerveController;
import swervelib.SwerveDrive;
import swervelib.SwerveModule;
import swervelib.imu.SwerveIMU;
import swervelib.math.SwerveMath;
import swervelib.parser.SwerveDriveConfiguration;
import swervelib.parser.SwerveParser;
import swervelib.telemetry.SwerveDriveTelemetry;
import swervelib.telemetry.SwerveDriveTelemetry.TelemetryVerbosity;

public class SwerveSubsystem extends SubsystemBase {
    private final SwerveDrive swerveDrive;
    private final double maxSpeed = Units.feetToMeters(15.5);
    private Translation2d centerOfRotation = new Translation2d();
    private final BooleanSupplier override;

    private Rotation2d RotationTargetOverride = new Rotation2d();
    private boolean RotationTargetOverrideEnabled = false;

    public SwerveSubsystem(File dir, BooleanSupplier override) {
        this.override = override;
        SwerveDriveTelemetry.verbosity = TelemetryVerbosity.HIGH;
        try {
            double driveConversionFactor = SwerveMath.calculateMetersPerRotation(Units.inchesToMeters(4), 6.75, 1);
            double steeringConversionFactor = SwerveMath.calculateDegreesPerSteeringRotation(21.42, 1);
            swerveDrive = new SwerveParser(dir).createSwerveDrive(maxSpeed, steeringConversionFactor, driveConversionFactor);
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
        setRotationCenter(new Translation2d());
        setHeadingCorrection(true);


        //set pathplanner rotation override method
        PPHolonomicDriveController.setRotationTargetOverride(this::getRotationTargetOverride);
        swerveDrive.setCosineCompensator(false);

    }

    /** The primary method for controlling the drivebase. Takes a Translation2d and a rotation rate, and calculates and commands module states accordingly. 
     * Can use either open-loop or closed-loop velocity control for the wheel velocities. Also has field- and robot-relative modes, 
     * which affect how the translation vector is used.
     * 
     * @param translation {@link Translation2d} that is the commanded linear velocity of the robot, in meters per second.
     * In robot-relative mode, positive x is torwards the bow (front) and positive y is torwards port (left). 
     * In field-relative mode, positive x is away from the alliance wall (field North) and positive y is torwards the left wall when looking 
     * through the driver station glass (field West).
     * @param rotation Robot angular rate, in radians per second. CCW positive. Unaffected by field/robot relativity.
     * @param feildRelative Drive mode. True for field-relative, false for robot-relative.
     * @param isOpenLoop Whether to use closed-loop velocity control. Set to true to disable closed-loop.
     */
    public void drive(Translation2d translation, double rotation, boolean feildRelative, boolean isOpenLoop) {
        swerveDrive.drive(translation, rotation, feildRelative, isOpenLoop, centerOfRotation);
    }

    /** Gets the max speed of swerve drive
     * 
     * @return The max speed of the robot in Meters/s
     */
    public double getMaxSpeed() {
        return maxSpeed;
    }

    /** Gets the point that the robot rotates around
     * 
     * @return The Robots Center of rotation as a {@link Translation2d}
     */
    public Translation2d getRotationCenter() {
        return centerOfRotation;
    }

    /** Sets the center of rotation the robot will rotate around
     * 
     * @param rotationCenter the point the robot will rotate around as a {@link Translation2d}
     */
    public void setRotationCenter(Translation2d rotationCenter) {
        centerOfRotation = rotationCenter;
    }

    public Rotation3d getGyroRotation() {
        return swerveDrive.getGyroRotation3d();
    }

    public void setHeadingCorrection(boolean headingCorrection) {
        swerveDrive.setHeadingCorrection(headingCorrection);
    }

    public boolean getHeadingCorrection() {
        return swerveDrive.headingCorrection;
    }

    /** Gets the current swerve drive kinematics
     * 
     * @return the current state of the swerve drive kinematics as {@link SwerveDriveKinematics}
     */
    public SwerveDriveKinematics getKinematics() {
        return swerveDrive.kinematics;
    }

    /** Gets the current module positions (azimuth and wheel position (meters)). Inverts the distance from each module if invertOdometry is true.
     * 
     * @return A list of {@link SwerveModulePosition}s containg the current module positions
     */
    public SwerveModulePosition[] getModulePositions() {
        return swerveDrive.getModulePositions();
    }

    /** Resets odometry to the given pose
     * 
     * @param pose The pose to set the odometry to
     */
    public void resetOdometry(Pose2d pose) {
        //setRotation(initialHolonomicPose.getRotation().getRadians());
        swerveDrive.resetOdometry(pose);
    }

    /** Set the expected gyroscope angle using a {@link Rotation3d} object. To reset gyro,
     * set to a new {@link Rotation3d} subtracted from the current gyroscopic readings {@link SwerveIMU#getRotation3d()}.
     * 
     * @param rotation
     */
    public void setRotation(double rotation) {
        swerveDrive.setGyro(new Rotation3d(0, 0, rotation));
    }

    /** Gets the current pose (position and rotation) of the robot, as reported by odometry.
     * 
     * @return The robot's pose
     */
    public Pose2d getPose() {
        return swerveDrive.getPose();
    }

    /** Set chassis speeds with closed-loop velocity control.
     * 
     * @param speeds Chassis speeds to set.
     */
    public void setChasisSpeeds(ChassisSpeeds speeds) {
        if(!override.getAsBoolean()) {
            swerveDrive.setChassisSpeeds(speeds);
        } else {
            swerveDrive.setChassisSpeeds(new ChassisSpeeds(0, 0, 0));
        }
    }

    /** Post the trajectory to the field
     * 
     * @param trajectory the trajectory to post.
     */
    public void postTrajectory(Trajectory trajectory) {
        swerveDrive.postTrajectory(trajectory);
    }

    /** 
     * Resets the gyro angle to zero and resets odometry to the same position, but facing toward 0.
     */
    public void zeroGyro() {
        swerveDrive.zeroGyro();
    }

    /**
     * Sets the drive motors to brake/coast mode.
     * @param brake True to set motors to brake mode, false for coast.
     */
    public void setMotorBrake(boolean brake) {
        swerveDrive.setMotorIdleMode(brake);
    }

    /**
     * Gets the current yaw angle of the robot, as reported by the imu. CCW positive, not wrapped.
     * @return The yaw as a {@link Rotation2d} angle
     */
    public Rotation2d getHeading() {
        return swerveDrive.getYaw();
    }

    /**
     * Gets the current pitch angle of the robot, as reported by the imu.
     * @return The heading as a {@link Rotation2d} angle
     */
    public Rotation2d getPitch() {
        return swerveDrive.getPitch();
    }

    /**
   * Gets the current roll angle of the robot, as reported by the imu.
   * @return The heading as a {@link Rotation2d} angle
   */
    public Rotation2d getRoll() {
        return swerveDrive.getRoll();
    }

    /**
     * Gets the current module states (azimuth and velocity)
     * @return list of {@link SwerveModuleState} containing the current module states
     */
    public SwerveModuleState[] getStates() {
        return swerveDrive.getStates();
    }

    public SwerveModule getFrontLeftModule() {
        return swerveDrive.getModuleMap().get("frontleft");
    }

    public SwerveModule getFrontRightModule() {
        return swerveDrive.getModuleMap().get("frontright");
    }

    public SwerveModule getBackLeftModule() {
        return swerveDrive.getModuleMap().get("backleft");
    }

    public SwerveModule getBackRightModule() {
        return swerveDrive.getModuleMap().get("backright");
    }

    public ArrayList<TalonFX> getMotors() {
        ArrayList<TalonFX> motors = new ArrayList<TalonFX>();
        for(SwerveModule module : swerveDrive.getModules()) {
            motors.add((TalonFX)module.getDriveMotor().getMotor());
            motors.add((TalonFX)module.getAngleMotor().getMotor());
        }
        return motors;
    }

    /**
   * Get the chassis speeds based on controller input of 1 joystick [-1,1] and an angle.
   *
   * @param xInput                     X joystick input for the robot to move in the X direction. X = xInput * maxSpeed
   * @param yInput                     Y joystick input for the robot to move in the Y direction. Y = yInput *
   *                                   maxSpeed;
   * @param angle                      The desired angle of the robot in radians.
   * @param currentHeadingAngleRadians The current robot heading in radians.
   * @param maxSpeed                   Maximum speed in meters per second.
   * @return {@link ChassisSpeeds} which can be sent to th Swerve Drive.
   */
    public ChassisSpeeds getTargetSpeeds(double xInput, double yInput, double angle, double currentHeading, double maxSpeed) {
        xInput = Math.pow(xInput, 3);
        yInput = Math.pow(yInput, 3);
        return swerveDrive.swerveController.getTargetSpeeds(xInput, yInput, angle, currentHeading, maxSpeed);
    }

    public ChassisSpeeds getTargetSpeeds(double xInput, double yInput, double headingX, double headingY) {
        Translation2d scaledInputs = SwerveMath.cubeTranslation(new Translation2d(xInput, yInput));
        return getSwerveController().getTargetSpeeds(scaledInputs.getX(), scaledInputs.getY(), headingX, headingY, getHeading().getRadians(), maxSpeed);
    }

    /**
   * Gets the current field-relative velocity (x, y and omega) of the robot
   *
   * @return A ChassisSpeeds object of the current field-relative velocity
   */
    public ChassisSpeeds getFieldVelocity() {
        return swerveDrive.getFieldVelocity();
    }

    /**
   * Gets the current robot-relative velocity (x, y and omega) of the robot
   *
   * @return A ChassisSpeeds object of the current robot-relative velocity
   */
    public ChassisSpeeds getRobotVelocity() {
        return swerveDrive.getRobotVelocity();
    }

    /**
     * Gets the Swerve controller for controlling heading of the robot.
     * @return The swerve Controller as a {@link SwerveController}
     */
    public SwerveController getSwerveController() {
        return swerveDrive.swerveController;
    }

    /**
     * Gets the current swerve drive configuration
     * @return The swerve Drive configuration as a {@link SwerveDriveConfiguration}
     */
    public SwerveDriveConfiguration getConfiguration() {
        return swerveDrive.swerveDriveConfiguration;
    }

    /**
   * Point all modules toward the robot center, thus making the robot very difficult to move. Forcing the robot to keep
   * the current pose.
   */
    public void lockPose() {
        swerveDrive.lockPose();
    }

    /**
   * Add a vision measurement to the {@link SwerveDrivePoseEstimator} and update the {@link SwerveIMU} gyro reading with
   * the given timestamp of the vision measurement.
   *
   * @param robotPose Robot {@link Pose2d} as measured by vision.
   * @param timeStamp Timestamp the measurement was taken as time since startup, should be taken from {@link Timer#getFPGATimestamp()} or similar sources.
   */
    public void addVisionMeasurement(Pose2d robotPose, double timeStamp) {
        swerveDrive.addVisionMeasurement(robotPose, timeStamp);
    }

    public Field2d getField() {
        return swerveDrive.field;
    }

    public ArrayList<Double> getMotorTemps() {
        ArrayList<Double> temps = new ArrayList<>();
        for(TalonFX motor : getMotors()) {
            temps.add(motor.getDeviceTemp().getValue());
        }
        return temps;
    }

    public double getMaximumAngularVelocity() {
        return swerveDrive.getMaximumAngularVelocity();
    }

    /**
   * Add a fake vision reading for testing purposes.
   */
    public void addFakeVisionReading() {
        addVisionMeasurement(new Pose2d(2.4, 4.7, Rotation2d.fromDegrees(180)), Timer.getFPGATimestamp());
    }

    public Command driveToPose(Pose2d pose, PathConstraints constraints) {
        return AutoBuilder.pathfindToPose(pose, constraints, 0, 0);
    }

    public Command driveToPose(Pose2d pose) {
        PathConstraints constraints = new PathConstraints(
            swerveDrive.getMaximumVelocity(), 4.0,
            swerveDrive.getMaximumAngularVelocity(), Units.degreesToRadians(720)
        );
        return driveToPose(pose, constraints);
    }

    public Command pathFindAndFollowPath(String path, PathConstraints constraints) {
        return AutoBuilder.pathfindThenFollowPath(PathPlannerPath.fromPathFile(path), constraints);
    }

    public Command pathfindAndFollowPath(String path) {
        PathConstraints constraints = new PathConstraints(
            swerveDrive.getMaximumVelocity(), 4.0,
            swerveDrive.getMaximumAngularVelocity(), Units.degreesToRadians(720)
        );
        return pathFindAndFollowPath(path, constraints);
    }
    
    public Command lockPoseCommand() {
        return Commands.run(() -> swerveDrive.lockPose(), this);
    }

    //test command to make sure swerve is odometry is oriented the correct way
    //should spin the robot counter clockwise
    //if not read the docs https://yagsl.gitbook.io/yagsl/configuring-yagsl/the-eight-steps
    public Command spinCounterClockwise() {
        return run(() -> setChasisSpeeds(new ChassisSpeeds(0,0,1)));
    }

    /**
     * Get the pathplanner Rotation override
     * @return an optional Rotation2d of the target override. will return empty if pathplanner will not override the rotation target
     */
    public Optional<Rotation2d> getRotationTargetOverride() {
        if(RotationTargetOverrideEnabled) {
            return Optional.of(RotationTargetOverride);
        } else return Optional.empty();
    }

    private Translation2d applyDeadband(double vX, double vY) {
        if(Math.abs(vX) < 0.2 && Math.abs(vY) < 0.2) {
            vX = 0;
            vY = 0;
        }

        return new Translation2d(vX, vY);
    }

    public Command driveCommand(DoubleSupplier vX, DoubleSupplier vY, DoubleSupplier omega, DoubleSupplier throttle, boolean feildRelative, boolean openLoop) {
        return run(() -> {
            Translation2d input = applyDeadband(vX.getAsDouble(), vY.getAsDouble());

            double speedMultiplier = MathUtil.clamp(throttle.getAsDouble(), 0.1, 1);

            double xVelocity = (input.getX() * getMaxSpeed()) * speedMultiplier;
            double yVelocity = (input.getY() * getMaxSpeed()) * speedMultiplier;
            double angVelocity = (Math.pow(MathUtil.applyDeadband(omega.getAsDouble(), 0.2), 3) * getSwerveController().config.maxAngularVelocity) * 0.7;
            Translation2d translation = new Translation2d(xVelocity, yVelocity);

            drive(translation, angVelocity, feildRelative, openLoop);
        });
    }

    public Command teleopCommand(DoubleSupplier vX, DoubleSupplier vY, DoubleSupplier omega, DoubleSupplier throttle) {
        return driveCommand(vX, vY, omega, throttle, true, false);
    }

    public Command RotatTwardsPose(DoubleSupplier vX, DoubleSupplier vY, DoubleSupplier throttle, Supplier<Pose2d> target, Supplier<Rotation2d> rotationOffset) {
        PIDController rotationPID = new PIDController(3, 0, 0);

        return Commands.runOnce(() -> rotationPID.reset()).andThen(run(() -> {
            Translation2d input = applyDeadband(vX.getAsDouble(), vY.getAsDouble());

            double maxAngularVelocity = getSwerveController().config.maxAngularVelocity * 1;
            double angVelocity = 0;
            if(target.get() != null) {
                Pose2d currentPose = getPose();
                Pose2d target_in_swerve_coordinates = target.get().relativeTo(currentPose);
                double rotation_error = Math.atan2(target_in_swerve_coordinates.getY(), target_in_swerve_coordinates.getX()) - rotationOffset.get().getRadians();
                if (rotation_error > Math.PI) rotation_error -= Math.PI * 2;
                if (rotation_error < -Math.PI) rotation_error += Math.PI * 2;
                angVelocity = MathUtil.clamp(-rotationPID.calculate(rotation_error), -maxAngularVelocity, maxAngularVelocity);
            }

            double speedMultiplier = MathUtil.clamp(throttle.getAsDouble(), 0.1, 1);
            double xVelocity = (input.getX() * getMaxSpeed()) * speedMultiplier;
            double yVelocity = (input.getY() * getMaxSpeed()) * speedMultiplier;
            Translation2d translation = new Translation2d(xVelocity, yVelocity);

            drive(translation, angVelocity, true, false);
        })).finallyDo(() -> rotationPID.close());
    }
}
